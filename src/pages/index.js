import * as React from "react"

// data
const nav__item_list = [
  {
    text: "Homepage",
    url: "/home",
  },
  {
    text: "About",
    url: "/about",
  },
]

// markup
const IndexPage = () => {
  return (
    <main>
      <title>Home Page</title>
      <h1>
        ITJ Dev
        <br />
      </h1>
    </main>
  )
}

export default IndexPage
